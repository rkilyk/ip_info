FROM ubuntu
MAINTAINER malrej96@gmail.com
ADD ./ip_info /usr/src/ip_info
ADD ./ip_info /bin/ip_info
RUN apt-get update && apt-get install -y --no-install-recommends  ntp
RUN apt-get update &&  apt-get -y install whois
RUN chmod +x /usr/src/ip_info
ENTRYPOINT ["/usr/src/ip_info"]
CMD []

